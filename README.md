# Polymer Test Container

A wee little docker image for testing Polymer web components

## example .gitlab-ci.yml

```yaml
job:
  image: registry.gitlab.com/bennyp/polymer-test
  script:
    - npm install
    - bower install
    - xvfb-run polymer test
```